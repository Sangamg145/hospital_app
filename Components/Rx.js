import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,FlatList,ScrollView, Pressable } from 'react-native'
import { ActivityIndicator } from 'react-native-paper';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import axios from 'axios';

function Rx({navigation}) {
    const [isLoading, setLoading] = useState(true);
    const [userdata, setUserdata] = useState([]);
    const [userdata1, setUserdata1] = useState([]);
  
    const Data1 = [{
      id:"1",
      name:"Diet",
      fill:userdata.diet_adherence,
      image:require('./Image/diet11.png'),
      a:'BreakFast (according to time)',
      b:'Warm Lemon Water',
      b1:'2 Glass',
      c:'Idli Sambar',
      c1:"Nos 2",
      d:'Fruit Chaat',
      d1:"2 Cup",
      name1:'Diet Completed'
   
   },
   {
      id:"2",
      name:"Exercise",
      fill:userdata.exercise_adherence,
      image:require('./Image/weightlifting11.png'),
      a:'Before BreakFast',
      b:'Sit Ups 10 Sets',
      b1:'Special Instruction',
      c:'Lunges',
      c1:"5 Sets",
      name1:'Exercise Completed'
   },
   {
      id:"3",
      name:"Madication",
      fill:userdata.medicine_adherence,
      image:require('./Image/pills11.png'),
      a:'after BreakFast',
      b:'Ecosprin - 75mg',
      b1:'Special Instruction',
      c:'Amlopress AT',
      c1:"As Doctor Instruction",
      name1:'Madication Completed'
     },
   {
      id:"4",
      name:"Wound Care",
      fill:userdata.wound_care_adherence,
      image:require('./Image/sling11.png'),
      a:'Before BreakFast',
      b:'Not Decided',
      b1:'Unknown',
      c:'no',
      c1:"unknows",
      name1:'Wound Care Completed'
     },
      {
         id:"5",
         name:"Diagnostic Test",
         fill:userdata.lab_test_adherence,
         image:require('./Image/Group11.png'),
         a:'Check Your Health',
         b:'Heart Check',
         b1:'Instruction',
         c:'Blood Check',
         c1:"unknown",
         name1:'Lab Test'
       },
      {
         id:"6",
         name:"Appointment",
         fill:userdata.appointment_adherence,
         image:require('./Image/calendar11.png'),
         a:'Cunsult Doctor',
         b:'Previous Doctor ',
         b1:'Doctor Name',
         c:'Cunsult New',
         c1:"New Doctor Name",
         name1:'Doctors Appointment'
       },
   ]
   
  useEffect(() => {
    const URL = "https://aiqahealth.com/doctor/api/adherence/"
    const Token = "57bbec1b20f1f33b5744090b26f3bed279239a35"
    const AuthStr = 'token '.concat(Token); 
      axios.get(URL, {
       headers: {
       'Authorization': `${AuthStr}` 
     }
        }) 
     .then(response => {
         console.log(response.data)
         setUserdata(response.data.data)
      }) .catch((error) => {
         console.log('error is ' + error);
      })}, []); 


      useEffect(() => {
        const URL = "https://aiqahealth.com/doctor/api/patient-exercise/"
        const Token = "57bbec1b20f1f33b5744090b26f3bed279239a35"
        const AuthStr = 'token '.concat(Token); 
          axios.get(URL, {
           headers: {
           'Authorization': `${AuthStr}` 
         }
            }) 
         .then(response => {
             console.log(response.data.today.total)
             setUserdata1(response.data.today)
          }) .catch((error) => {
             console.log('error is ' + error);
          })}, []); 

    const renderItem = ({ item }) =>
     (
      <Pressable style={{width:164,height:168,paddingHorizontal:8,
         paddingVertical:12,borderRadius:10,marginBottom:20,backgroundColor:'#fff'
 }}
 onPress={()=>navigation.navigate("Diet", {
  itemId: item.name,
  a:item.a,
  b:item.b,
  b1:item.b1,
  c:item.c,
  c1:item.c1,
  d:item.d,
  d1:item.d1,
  name1:item.name1,
  ex:userdata1.total
})}>
      <View style={{paddingHorizontal:20}}>
      <AnimatedCircularProgress
     size={108}
     width={11}
     fill={item.fill}
     tintColor="#1E6785"
     backgroundColor="#D5ECF6"
     
      />
      <View style={{width:65,height:65,position:'absolute',left:40,top:20,justifyContent:'center'}}> 
      <Image style={{width:44,height:44,alignSelf:'center'}}  source={item.image} />
      </View>
    
      </View>
      <Text style={{textAlign:'center',marginTop:10,fontSize:18,fontWeight:'bold'}}>{item.name}</Text>
   </Pressable>
   
    );
  
    return (
      <ScrollView style={{backgroundColor:'#fff'}}> 
        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:20}}>
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={80}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>M</Text>
      </View>
      
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={50}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={20}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
       <View style={{backgroundColor:'#1E6785',borderRadius:40,width:27,height:27,alignSelf:'center',marginTop:4}}>
       <Text style={{textAlign:'center',marginTop:4,color:'#fff'}}>W</Text>
       </View>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>
  
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>F</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
         <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
        <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>
        </View>
  
  <Text style={{fontSize:16,marginTop:8,marginLeft:20,fontWeight:'bold'}}>Dr. Harshit Suneja (ENT)</Text>
  <Text style={{fontSize:12,marginTop:2,marginBottom:20,marginLeft:20}}>Max Hospital Saket</Text>
<View style={{backgroundColor:'rgba(212, 217, 232, 0.1)'}}>
<FlatList
        data={Data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        numColumns={2}
        columnWrapperStyle={{justifyContent: 'space-evenly'}}
      />
</View>
   </ScrollView>
    );
  }
export default Rx

const styles = StyleSheet.create({})
