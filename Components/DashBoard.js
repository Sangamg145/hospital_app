import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,ScrollView,Dimensions } from 'react-native'
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const DashBoard = ({navigation}) => {
const [data1, setData1] = useState([])

useEffect(() => {
  const URL = "http://13.126.80.252/accounts/api/patient-detail/"
  const Token = "8f94fc3e9d82c09be0f1aebd1c4d49b211a9a549"
  const AuthStr = 'token '.concat(Token); 
  axios.post(URL, { headers: { Authorization: AuthStr } })
   .then(response => {
    setData1(response.data.status)
       console.log(response.data);
    })
    
   .catch((error) => {
       console.log('error ' + error);
    }); 
}, []);


    return (
        <ScrollView style={{backgroundColor:'#fff'}}>
        <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:16,marginTop:16}}>
          <Text style={{fontSize:22,fontWeight:'bold'}}>Hello Harshit Suneja</Text>
        <Image style={{width:35,height:40,borderRadius:70,alignSelf:'center',resizeMode:'contain'}} 
          source={require('./Image/Frame170.png')} />
        </View>
        <Text style={{fontSize:16,marginHorizontal:16}}>Keep Tracking Your Health</Text>

        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginHorizontal:16,marginTop:16}}>
          <View style={{width:122,height:108,flexDirection:'row'}}>
<Text style={{fontSize:72,color:'rgba(236, 176, 45, 1)',fontWeight:'bold'}}>85</Text>
<Text style={{fontSize:56,color:'rgba(236, 176, 45, 1)'}}>%</Text>
          </View>
         
        <View>
        <View style={{width:149,height:108,marginTop:4}}>
<Text style={{fontSize:20,color:'rgba(77, 77, 79, 1)',fontWeight:'bold'}}>This Week Adherances</Text>
<Text style={{fontSize:14,color:'rgba(77, 77, 79, 1)',marginVertical:20}}>5% Up from last Week</Text>
          </View>
        </View>
        </View>


        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginHorizontal:16,marginTop:16}}>
          <View style={{width:100,height:30,borderWidth:1,borderRadius:10}}>
<Text style={{marginTop:5,fontSize:13,color:'rgba(26, 176, 45, 1)',fontWeight:'bold',textAlign:'center'}}>Today</Text>
          </View>
        <View style={{width:100,height:30,borderWidth:1,borderRadius:10}}>
<Text style={{marginTop:5,fontSize:13,color:'rgba(77, 77, 79, 1)',fontWeight:'bold',textAlign:'center'}}>This Week</Text>
        </View>
        <View style={{width:100,height:30,borderWidth:1,borderRadius:10}}>
<Text style={{marginTop:5,fontSize:13,color:'rgba(77, 77, 79, 1)',fontWeight:'bold',textAlign:'center'}}>This Month</Text>
        </View>
        </View>
        <LinearGradient
                colors={['rgba(255,12,0,0.7)', 'rgba(255,0,0,0.8)']} 
                style={{height:60,
        paddingHorizontal:12,paddingVertical:12,borderRadius:10,marginHorizontal:16,marginTop:25}}>
<Text style={{color:'#fff',paddingHorizontal:16}}>It looks like you have high blood pressure. Take rest or consult doctor now</Text>
        </LinearGradient>


<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:24,marginHorizontal:16}}>
<View>
  <Text>Blood Pressure</Text>
  <View style={{width:164,height:232,backgroundColor:'#000000',padding:12,marginTop:12,
  borderWidth:1,borderRadius:8,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#fff'}}>SYS</Text>
        <View style={{ width:20,height:20,backgroundColor:'#fff',borderRadius:20}}>
      </View>
</View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
      <Text style={{color:'#fff',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(250, 253, 253, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginTop:24}}>
        <Text style={{color:'#fff'}}>160</Text>
        <View style={{ borderStyle:'dotted', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>140</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>120</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('./Image/Vector1.png')} />
</View>
</View>
</View>


<View>
  <Text>120 mmHg/ 80mmHg</Text>
  <View style={{width:164,height:232,backgroundColor:'#000000',padding:12,marginTop:12,
  borderWidth:1,borderRadius:8,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#fff'}}>DIA</Text>
        <View style={{ width:20,height:20,backgroundColor:'#fff',borderRadius:20}}>
      </View>
</View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
      <Text style={{color:'#fff',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(250, 253, 253, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginTop:24}}>
        <Text style={{color:'#fff'}}>160</Text>
        <View style={{ borderStyle:'dotted', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>140</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>120</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('./Image/Vector1.png')} />
</View>
  
        </View>

</View>
</View>

<View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:12}}>
<Text style={{fontWeight:'bold',fontSize:24}}>Heartbeat</Text>
<Text style={{fontSize:20}}>62 BPM</Text>
</View>
<View style={{height:213,padding:12,}}>
<Text style={{marginVertical:7}}>100</Text>
<Text style={{marginVertical:7}}>80</Text>
<Text style={{marginVertical:7}}>70</Text>
<Text style={{marginVertical:7}}>60</Text>
<Text style={{marginVertical:7}}>50</Text>
<Text style={{marginVertical:7}}>40</Text>
<Image style={{height:129,
        alignSelf:'center',position:'absolute',left:35,top:66}}  source={require('./Image/Vector1.png')} />

</View>
        </ScrollView>
    )
}

export default DashBoard

const styles = StyleSheet.create({})
