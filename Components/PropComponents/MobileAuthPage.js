import React, { useState } from 'react';
import { Button, TextInput, View,ActivityIndicator } from 'react-native';
import auth from '@react-native-firebase/auth';


export default function MobileAuthPage(props) {
  const [number, setnumber] = useState('')
  const [confirm, setConfirm] = useState(null);
  const [load, setLoad] = useState(false);
  const [code, setCode] = useState('');


  const signin = async ()=>{ 
    setLoad(true)
    const confirmation = await auth().signInWithPhoneNumber('+91'+number)
    if(confirmation){
     setConfirm(confirmation) 
      console.log(confirm)
     setLoad(false) 
      props.navigation.navigate("VerificationCode",{confirm12:confirmation})
    }
  }

/*   async function confirmCode() {
    try {
      await confirm.confirm(code);
    } catch (error) {
      console.log('Invalid code.');
    }
  } */

  /* if (!confirm) {
    return (
      <Button
        title="Phone Number Sign In"
        onPress={() => signInWithPhoneNumber('+91 7007441554')}
      />
    );
  } */

  return (
    <>
     <View>
       {
         load ? <ActivityIndicator size={60} color="tomato" style={{marginTop:150}} />:<View>
            <TextInput value={number}
      placeholder="Enter 10 Digit Mobile No "
      keyboardType="number-pad"
      maxLength={10}
       onChangeText={(value) => setnumber(value)} 
       style={{width:'100%',borderBottomWidth:1,marginTop:20,marginBottom:20}}
       />


      <Button title="Confirm Code" onPress={signin}
      disabled={number.length==10 ? false:true}
      />
         </View>
       }
     </View>
    </>
  );
}