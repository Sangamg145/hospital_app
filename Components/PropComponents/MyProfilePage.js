import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,FlatList,ScrollView, Pressable } from 'react-native'
import { ProgressBar, Colors } from 'react-native-paper';
import axios from 'axios';
const Data = [{
    id:1,
    name:"My Plan",
    spec:'Going to Hospital',
    img:require('../Image/Frame170.png')
    
},
{
    id:2,
    name:"My Doctor",
    spec:'Treating Doctor',
    img:require('../Image/Group1.png')
   
},
{
    id:3,
    name:"My Appointments",
    spec:'View Previous Appointment',
    img:require('../Image/Group1.png')

  },
  {
    id:4,
    name:"My Reports",
    spec:'Add or Change Your Medical Records',
    img:require('../Image/Frame171.png')
    
},
{
    id:5,
    name:"Payment History",
    spec:'Previous Payment',
    img:require('../Image/Frame172.png')
   
},
{
    id:6,
    name:"Contact Us",
    spec:'Call, Chat, Email or Send Queries to our Team',
    img:require('../Image/Frame173.png')

  },
  {
    id:7,
    name:"Refer & Earn",
    spec:'Refer your Friend and Earn Money',
    img:require('../Image/Frame174.png')
    
},
{
    id:8,
    name:"Privacy Policy",
    spec:'Privacy terms of Appointments, Lab Test',
    img:require('../Image/Frame175.png')
   
},
{
    id:9,
    name:"Terms & Conditions",
    spec:'Privacy terms of Appointments, Lab Test',
    img:require('../Image/Frame175.png')

  },
]

const MyProfilePage = ({navigation}) => {
  const [userdata, setUserdata] = useState([]);
  const [userdata1, setUserdata1] = useState([]);

useEffect(() => {
  const URL = "https://aiqahealth.com/accounts/api/patient-detail/"
  const Token = "57bbec1b20f1f33b5744090b26f3bed279239a35"
  const AuthStr = 'token '.concat(Token); 

    axios.post(URL, {
      data: {
          name: "Sangam",
          email: "Sg@gmail.com",
          dob: "10/10/2000",
          gender: "male",
          city: "Delhi"
      }
        }, {
     headers: {
     'Authorization': `${AuthStr}` 
   }
      }) 
   .then(response => {
       console.log(response.data.data.name)
       setUserdata(response.data.data)
       setUserdata1(response.data.data1)
    })
    
   .catch((error) => {
       console.log('error is ' + error);
    });
     
}, []); 


    const renderItem = ({ item }) => (     
<View>
<View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:38}}>
  <View style={{flexDirection:'row'}}>
  <Image style={{width:25,height:25,marginRight:10,resizeMode:'center'}}  
   source={item.img} />
 <View>
 <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>{item.name}</Text>
   <Text style={{fontSize:12,marginTop:8}}>{item.spec}</Text>
 </View>
  </View>
   <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:25}}  
   source={require('../Image/navigateright.png')} />
   </View>
   <View style={{
    borderStyle: 'dashed',
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor:'rgba(0, 0, 0, 0.25)',
 
    height:0,
    marginHorizontal:30,
    top:20
  }}></View>
</View>
    )


    return (
        <ScrollView style={{backgroundColor:'#fff'}}>
         <Image style={{width:135,height:135,borderRadius:70,alignSelf:'center',marginTop:7}} 
          source={{uri:"https://i.picsum.photos/id/1005/5760/3840.jpg?hmac=2acSJCOwz9q_dKtDZdSB-OIK1HUcwBeXco_RMMTUgfY"}} />
 <Pressable onPress={()=>navigation.navigate('ProfileUpdate',{
  id: userdata.id,
  name:userdata.name,
  dob:userdata.dob,
 gender:userdata.gender,
 email:userdata.email,
 city:userdata1.city,
 mobile:userdata.mobile
})}>
   <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
  <View   onPress={()=>navigation.navigate("Home")}>
  <Text style={{fontSize:20,color:"#1E6785",fontWeight:'bold'}}>{userdata.name}</Text>
   <Text style={{fontSize:12,marginTop:8}}>Update Your Profile</Text>
  </View>
   <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:15}} 
   source={require('../Image/navigateright.png')} />
   </View>
<View style={{paddingHorizontal:10,width:'80%'}}>
<ProgressBar progress={0.66} color="#1E6785" style={{height:8,borderRadius:10}} />
<Text style={{fontSize:10,color:"#1E6785",marginTop:4}}>66% Complete</Text>
</View>
</Pressable>
<View style={{
    borderStyle: 'dashed',
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor:'rgba(0, 0, 0, 0.25)',
    width:"100%",
    height:0,
    top:25
  }}>
</View>


<FlatList
        style={{marginBottom:20}}
        data={Data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        showsHorizontalScrollIndicator={false}
      />     

        </ScrollView>
    )
}

export default MyProfilePage

const styles = StyleSheet.create({})
