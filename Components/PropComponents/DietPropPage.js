import React,{useState,useEffect} from 'react'
import {StyleSheet, Text, View,Image,FlatList,ScrollView, Pressable,ActivityIndicator } from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import axios from 'axios';
import { FAB } from 'react-native-paper';

const DietPropPage = ({route,navigation}) => {
    const [data12,setData12] = useState([])
    const [data13,setData13] = useState([])
    const { itemId,a,b,b1,c,c1,d,d1,name1,ex } = route.params;
    const [isLoading, setLoading] = useState(false);
    const [isActive, setActive] = useState(true);

    useEffect(() => {
        const URL = "http://00676f3a65ca.ngrok.io/doctor/api/patient-diet/"
        const Token = " 0d85c132523fdca6494ff1b786b4b3c1d6de6626"
        const AuthStr = 'token '.concat(Token); 
        axios.get(URL, { headers: { Authorization: AuthStr } })
         .then(response => {
          setData12(response.data.data)
          setData13(response.data.today.total)
             console.log(response.data.today.total);
             setLoading(false)
          })
          
         .catch((error) => {
             console.log('error ' + error);
          });
           
      }, []);
    

      const renderItem = ({ item }) => (
        <Pressable style={{width:164,height:168,paddingHorizontal:8,
           paddingVertical:12,borderRadius:10,marginBottom:20,backgroundColor:'#fff'
   }}
   onPress={()=>navigation.navigate("Diet")}>
        <View style={{paddingHorizontal:20}}>
        <AnimatedCircularProgress
       size={108}
       width={11}
       fill={item.fill}
       tintColor="#1E6785"
       backgroundColor="#D5ECF6"
       
        />
        <View style={{width:65,height:65,position:'absolute',left:40,top:20,justifyContent:'center'}}> 
        <Image style={{width:44,height:44,alignSelf:'center'}}  source={item.image} />
        </View>
      
        </View>
        <Text style={{textAlign:'center',marginTop:10,fontSize:18,fontWeight:'bold'}}>{item.name}</Text>
     </Pressable>
     
      );
    return (
        <View style={{backgroundColor:'#fff',height:"100%"}}>
                
                  {isLoading ? <View style={{marginTop:"50%"}}>
                    <ActivityIndicator size={60} color="tomato" />
                    <Text style={{textAlign:'center',fontSize:15}}>Loading Please Wait </Text>
                  </View> : (
        <ScrollView>
              <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:20}}>
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={80}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>M</Text>
      </View>
      
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={50}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={20}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
       <View style={{backgroundColor:'#1E6785',borderRadius:40,width:27,height:27,alignSelf:'center',marginTop:4}}>
       <Text style={{textAlign:'center',marginTop:4,color:'#fff'}}>W</Text>
       </View>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>
  
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>F</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
         <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={3}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
        <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>
        </View>
  

<View style={{flexDirection:'row',marginTop:24,marginLeft:18}}>  
  <Text style={{fontSize:24,fontWeight:'bold'}}>{data13}{ex}/{ex}</Text>
  <Text style={{marginTop:6}}> {name1}</Text>
  
</View>


<View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
<Pressable onPress={()=>navigation.navigate('MobileAuthPage')} 
style={{width:76,height:76,paddingHorizontal:4,
         paddingVertical:4,borderRadius:10,marginBottom:20,borderWidth:0.3}}>
      <View style={{paddingHorizontal:20}}>
      <View style={{width:35,height:35,justifyContent:'center'}}> 
      <Image style={{width:32,height:28,alignSelf:'center'}}  source={require('../Image/Vector31.png')} />
      </View>
      </View>
      <Text style={{textAlign:'center',marginTop:2,fontSize:12,fontWeight:'bold'}}>Breakfast</Text>
      <Text style={{textAlign:'center',fontSize:8,fontWeight:'bold'}}>8:00am-11:00am</Text>
   </Pressable>
   <Pressable style={{width:76,height:76,paddingHorizontal:2,
         paddingVertical:4,borderRadius:10,marginBottom:20,borderWidth:0.3,marginLeft:5}} >
      <View style={{paddingHorizontal:20}}>
      <View style={{width:35,height:35,justifyContent:'center'}}> 
      <Image style={{width:32,height:28,alignSelf:'center'}}  source={require('../Image/Vector32.png')} />
      </View>
      </View>
      <Text style={{textAlign:'center',marginTop:2,fontSize:12,fontWeight:'bold'}}>Lunch</Text>
      <Text style={{textAlign:'center',fontSize:8,fontWeight:'bold'}}>12:00pm-16:00pm</Text>
   </Pressable>
   <Pressable style={{width:76,height:76,paddingHorizontal:2,
         paddingVertical:4,borderRadius:10,marginBottom:20,borderWidth:0.3,marginLeft:5}}>
      <View style={{paddingHorizontal:20}}>
      <View style={{width:35,height:35,justifyContent:'center'}}> 
      <Image style={{width:32,height:28,alignSelf:'center'}}  source={require('../Image/Vector33.png')} />
      </View>
      </View>
      <Text style={{textAlign:'center',marginTop:2,fontSize:12,fontWeight:'bold'}}>Evening</Text>
      <Text style={{textAlign:'center',fontSize:8,fontWeight:'bold'}}>17:00pm-19:00pm</Text>
   </Pressable>
   <Pressable style={{width:76,height:76,paddingHorizontal:2,
         paddingVertical:4,borderRadius:10,marginBottom:20,borderWidth:0.3,marginLeft:5}}>
      <View style={{paddingHorizontal:20}}>
      <View style={{width:35,height:35,justifyContent:'center'}}> 
      <Image style={{width:32,height:32,alignSelf:'center'}}  source={require('../Image/Vector34.png')} />
      </View>
      </View>
      <Text style={{textAlign:'center',marginTop:2,fontSize:12,fontWeight:'bold'}}>Dinner</Text>
      <Text style={{textAlign:'center',fontSize:8,fontWeight:'bold'}}>20:00pm-22:00pm</Text>
   </Pressable>
</View>


<Text style={{fontWeight:'bold',fontSize:20,marginLeft:20}}>{a}</Text>
 

<View>
<View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:38}}>
  <View style={{flexDirection:'row'}}>
  <Image style={{width:30,height:30}}  
   source={require('../Image/water1.png')} />
 <View>
 <Text style={{fontSize:14,color:"#1E6785",marginLeft:10}}>{b}</Text>
   <Text style={{fontSize:10,marginLeft:10}}>{b1}</Text>
 </View>
  </View>
   <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:25,marginTop:10}}  
   source={require('../Image/navigateright.png')} />
   </View>
   <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Image style={{width:30,height:30}}  
   source={require('../Image/diet11.png')} />
 <View>
 <Text style={{fontSize:14,color:"#1E6785",marginLeft:10}}>{c}</Text>
   <Text style={{fontSize:10,marginLeft:10}}>{c1}</Text>
 </View>
  </View>
   <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:25,marginTop:10}}  
   source={require('../Image/navigateright.png')} />
   </View>
   <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Image style={{width:30,height:30}}  
   source={require('../Image/diet11.png')} />
 <View>
 <Text style={{fontSize:14,color:"#1E6785",marginLeft:10}}>{d}</Text>
   <Text style={{fontSize:10,marginLeft:10}}>{d1}</Text>
 </View>
  </View>
   <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:25,marginTop:10}}  
   source={require('../Image/navigateright.png')} />
   </View>
</View>

<FlatList
            data={data12}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
              <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Image style={{width:30,height:30}}  
   source={require('../Image/water1.png')} />
 <View>
 <Text style={{fontSize:18,color:"tomato",marginLeft:10}}>{item.time_of_food}</Text>
 <Text style={{fontSize:16,color:"#1E6785",marginLeft:10}}>{item.food_item}</Text>
   <Text style={{fontSize:14,marginLeft:10}}>{item.quantity}</Text>
  </View>
  </View>
  <Image style={{width:15,height:15,resizeMode:'contain',paddingHorizontal:25,marginTop:10}}  
   source={{uri:'https://www.clipartmax.com/png/middle/212-2121718_check-mark-computer-icons-customer-service-theme-clip-green-tick-icon-png.png'}}/>
   </View>
    )}
          /> 
       
        </ScrollView>
                  )}
        <FAB
        style={{ position: 'absolute',
        margin: 20,
        right: 10,
        bottom: 0,padding:8}}
        small
        icon="plus"
        onPress={() => console.log('Pressed')}
      />
      </View>   
   
    )
}

export default DietPropPage

const styles = StyleSheet.create({})
