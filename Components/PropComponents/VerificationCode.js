import React,{useState} from 'react'
import { ActivityIndicator } from 'react-native';
import { ToastAndroid } from 'react-native';
import { StyleSheet, Text, View,TextInput  } from 'react-native'
import { FAB } from 'react-native-paper';
const VerificationCode = ({navigation,route}) => {
    const [number, setnumber] = useState('')
    const [load, setLoad] = useState(false);
    const {confirm12} = route.params;
const otpverify = async ()=>{
   
    try{
        setLoad(true)
        let data = await confirm12.confirm(number) 
        console.log("data",data)
        setLoad(false)
        navigation.navigate("Home")
    } catch(error){
        console.log('Invalid Code')
       
        ToastAndroid.show('invalid code ',ToastAndroid.SHORT)
    } setLoad(false)
    
}

    return (
        <View>
         {load? <ActivityIndicator size={60} color="tomato" style={{marginTop:150}} />:<View>
            <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:50}}>
            <TextInput placeholder="Enter 6 Digit O T P"
               keyboardType='number-pad' 
               value={number}
               onChangeText={(value) => setnumber(value)} 
               maxLength={6}
               style={{ height: 40,width:220,margin: 12,borderWidth: 1,borderRadius:10}}/>
               </View>
            
         <FAB
         style={{  position: 'absolute',
         margin: 16,
         right: 20,
         top: 120,}}
         disabled={number.length==6 ? false:true}
         large
         icon="arrow-right"
         onPress={otpverify}
       />
             </View>}
        </View>
    )
}

export default VerificationCode

const styles = StyleSheet.create({})
