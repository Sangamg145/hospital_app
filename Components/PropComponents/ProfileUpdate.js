import React from 'react'
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'

const ProfileUpdate = ({navigation,route}) => {
    const { id,name,dob,gender,email,city,mobile} = route.params;
    return (
        <ScrollView>
               <Image style={{width:135,height:135,borderRadius:70,alignSelf:'center',marginTop:7}} 
          source={{uri:"https://i.picsum.photos/id/1005/5760/3840.jpg?hmac=2acSJCOwz9q_dKtDZdSB-OIK1HUcwBeXco_RMMTUgfY"}} />
 
        <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>Name</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{name}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>

          <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>Date Of Birth</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{dob}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>
          <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>Gender</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{gender}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>
          <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>Email Address</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{email}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>
          <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>City</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{city}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>
          <View  style={{padding:10,flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
          <View style={{flexDirection:'row'}}>
        
         <View>
         <Text style={{fontSize:16,color:"#1E6785",fontWeight:'bold'}}>Mobile No.</Text>
           <Text style={{fontSize:14,marginTop:8,fontWeight:'bold'}}>{mobile}</Text>
         </View>
          </View>
           </View>
           <View style={{
            borderStyle: 'dashed',
            borderWidth: 0.5,
            borderRadius: 1,
            borderColor:'rgba(0, 0, 0, 0.25)',
         
            height:0,
            marginHorizontal:30,
            top:10
          }}>
          </View>
        </ScrollView>
    )
}

export default ProfileUpdate

const styles = StyleSheet.create({})
