import React,{useState,useEffect} from 'react'
import { View, StyleSheet, Image, TouchableOpacity,Text,Button,ScrollView } from 'react-native'
import { TextInput } from 'react-native-paper';
import { Formik } from 'formik';
import Icon from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
const ProfileForme = ({data, udpateProfileData,navigation}) => {
    const [checked, setChecked] = useState('');
    const [userdata, setUserdata] = useState([]);
    const [name, setname] = useState('');
    const [email, setemail] = useState('');
    const [gender, setgender] = useState('');
    const [city, setcity] = useState('');
    const [dob, setdob] = useState('');
 
const submitt = ()=>{   
    const URL = "https://aiqahealth.com/accounts/api/patient-detail/"
    const Token = "57bbec1b20f1f33b5744090b26f3bed279239a35"
    const AuthStr = 'token'.concat(Token); 

      axios.post(URL, {
        data: data1
          }, {
       headers: {
       'Authorization': `${AuthStr}` 
     }
        }) 

     .then(response => {
         console.log(response)
         setUserdata(response.data.data)
      })
      
     .catch((error) => {
         console.log('error is =' + error);
      });
       
  
}

    
        const data1 = {
            name: name,
            email: email,
            dob: dob,
            gender: gender,
            city: city
        }
        




    return (
        <ScrollView showsVerticalScrollIndicator={false} style={{ marginBottom: 50,marginHorizontal:20 }}>
       
                <View>
                    <View style={styles.inputContainer}>
                        <TextInput
                           onChangeText={(value) => setname(value)}
                           value={name}
                            label="Name"
                            mode="outlined"
                            placeholder="Name"
                           
                        />
                    </View>
                    <Image style={styles.image} source={require('../Image/navigateright.png')} />

                    <View style={styles.inputContainer}>
                        <TextInput
                            right={<TextInput.Icon name={() => <Icon color='#f0f' name="calendar-today" size={20} />} />}
                            onChangeText={(value) => setdob(value)}
                          
                            value={dob}
                            label="DOB"
                            mode="outlined"
                            placeholder="DOB"
                            
                        />
                    </View>
                    <Image style={styles.image} source={require('../Image/navigateright.png')} />
                    <View style={styles.inputContainer}>
                        <Text style={{ marginBottom: 10 }} fontWeight="bold">
                            Gender
                        </Text>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={(value) => setgender(value)}
                                

                               
                            >
                                <Text>
                                    Male
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={(value) => setgender(value)}
                                value={gender}
                 
                            >
                                <Text  color='red'>
                                    Female
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={(value) => setgender(value)}
                            
                            >
                                <Text  >
                                    Other
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Image style={styles.image} source={require('../Image/navigateright.png')} />
                    <View style={styles.inputContainer}>
                        <TextInput
                            onChangeText={(value) => setemail(value)}
                         
                           value={email}
                            label="Email Address"
                            mode="outlined"
                            placeholder="Email Address"
                           
                        />
                    </View>
                    <Image style={styles.image} source={require('../Image/navigateright.png')} />
                    <View style={styles.inputContainer}>
                        <TextInput
                           onChangeText={(value) => setcity(value)}
                            value={city}
                            label="City"
                            mode="outlined"
                            placeholder="City"
                       
                        />
                        <Image style={styles.image} source={require('../Image/navigateright.png')} />
                    </View>
                    <Button onPress={submitt} title="Submit" /> 
                </View>
     

    </ScrollView>

    )
}

export default ProfileForme

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 2,
        marginVertical: 10,
    },
    inputContainer: {
        paddingVertical: 15,
    },
    radioButton: {
        borderWidth: 1,
        padding: 15,
        borderRadius: 5,
        marginRight: 20,
       
    },
    active: {
        color:'#fff',
        backgroundColor:'rgba(30, 103, 133, 1)'
    },

})
