import * as React from 'react';
import {StyleSheet,View,Text,Image,Dimensions, TouchableOpacity} from 'react-native';
const {width, height} = Dimensions.get('window')
const HomePage = ({navigation}) => (
  <View style={styles.container}>
    <View style={styles.container1}>
<TouchableOpacity onPress={()=>navigation.navigate('MyProfilePage')}
 style={{width:65,height:65,borderWidth:1,left: 18.5,top: 22,borderRadius:40,borderColor:'#1E6785'}}>
<Image style={{width:62,height:62}} source={require('./Image/userlogo.png')} />
</TouchableOpacity>
   <Text style={{width:148,height:36,left: 32.5,
   fontWeight:'100',fontSize:24,
   color:'#333333',fontWeight:'bold',
top: 24,}}>Hello Harshit</Text>

<Text style={{width:148,height:36,left: -115,color:'#4F4F4F',
top: 54,}}>Have a Great Day !</Text>
<Image
 style={{width:27,height:27,position:'absolute',right:55,
top: 26.5}} source={require('./Image/calendar.png')} />
<Image style={{width:26,height:26,position:'absolute',right:18,
top: 27.5}} source={require('./Image/bell.png')} />
    </View>
  </View>
);

export default HomePage;

const styles=StyleSheet.create({
  container:{
    width: "100.2%",
    height: 114,
    borderColor: '#000',
    borderWidth: 0.3,
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,    
  },
  container1:{
    top: 14,
    flexDirection:'row',

    
  },
  icon:{
    flexDirection:'row',
    marginHorizontal:10
  },
  devider:{
    marginTop:20
  },
  name:{
width: 148,
height: 36,
left: 88,
top: 68,

fontWeight:'600',
fontSize: 24,

  }
  
})