import React,{useState} from 'react'
import { View, Text,Image,ScrollView,Dimensions,Modal,StyleSheet,Pressable,Animated } from 'react-native'
import { FAB} from 'react-native-paper';
const {width, height} = Dimensions.get('window')
export default function Dailylogs() {
    const [modalVisible, setModalVisible] = useState(false);

    
    return (
        <View style={{backgroundColor:'#fff'}}>

<Modal
      
        animationType="slide"
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}>
        <View >
          <ScrollView style={styles.modalView}>
          <Text style={{fontSize:20,marginTop:14,}}>My Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>



<Text style={{fontSize:20,marginTop:24,}}>Other Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>
 

<Text style={{fontSize:20,marginTop:14,}}>My Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>



<Text style={{fontSize:20,marginTop:24,}}>Other Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>
 
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Hide Page</Text>
            </Pressable>
            </ScrollView>
          </View>
      </Modal>
  

        <ScrollView style={{paddingHorizontal:16}}>


            <Text style={{fontSize:20,marginTop:16}}>Symptom</Text>
<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} 
style={{flexDirection:'row',marginTop:12}}>
<Pressable style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}
 onPress={() => setModalVisible(true)}>
<Image source={require('./Image/dailylog1.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>NauSea</Text>
</Pressable>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('./Image/dailylog2.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Breathlessness</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('./Image/dailylog3.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8,textAlign:'center'}}>Raising Heart Beat</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('./Image/dailylog3.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8,textAlign:'center'}}>Raising Heart Beat</Text>
</View>
</ScrollView>

<Text style={{fontSize:20,marginTop:24}}>Vitals</Text>
<View style={{flexDirection:'row',marginTop:12}}>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('./Image/dailylog4.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Blood Pressure</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('./Image/dailylog5.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Temprature</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('./Image/dailylog6.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Weight</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('./Image/dailylog6.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Weight</Text>
</View>
</View>

<Text style={{fontSize:20,marginTop:24,}}>My Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>



<Text style={{fontSize:20,marginTop:24,}}>Other Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('./Image/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>
        </ScrollView>
            <View style={{position:'absolute',bottom:15,right:15,width:56,height:56,borderRadius:30,backgroundColor:'rgba(30, 103, 133, 1)'}} >
<View style={{alignSelf:'center',justifyContent:'center'}}>
<Text style={{fontSize:35,color:'#fff',textAlign:'center'}}>+</Text>
</View>
</View>
        </View>
    )
}
const styles = StyleSheet.create({
    centeredView: {
      flex: 1,

    },
    modalView: {
      marginTop:180,
      backgroundColor: "#fff",
   borderTopRightRadius:20,
   borderTopLeftRadius:20,
      padding: 35,
      shadowColor: "#000",
      borderWidth:1,
      width:'100.2%'
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
      marginBottom:50
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });
  