import React, {Component,useEffect,useState} from 'react';  
import {Platform, StyleSheet, Text, View,ScrollView,Image,TouchableOpacity,Button, Pressable,FlatList} from 'react-native'; 
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import BottomHome from './Components/BottomHome';
import FlatData from './Components/FlatData';
import FlatDataList from './Components/FlatDataList';
import GraphPage from './Components/GraphPage';
import HomePage from './Components/HomePage';
import Dailylogs from './Components/Dailylogs';
import DashBoard from './Components/DashBoard';
import Rx from './Components/Rx';
import { createStackNavigator } from '@react-navigation/stack';
import auth from '@react-native-firebase/auth';
import { ActivityIndicator } from 'react-native-paper';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import DietPropPage from './Components/PropComponents/DietPropPage';
import ProfileUpdate from './Components/PropComponents/ProfileUpdate';
import MobileAuthPage from './Components/PropComponents/MobileAuthPage';
import VerificationCode from './Components/PropComponents/VerificationCode';
import MyProfilePage from './Components/PropComponents/MyProfilePage';
import ProfileForme from './Components/PropComponents/ProfileForme';
import BottomTabNav from './src/navigation/BottomTabNav';
const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();



function HomeScreen({navigation}) {
  return (
    <ScrollView style={{backgroundColor:'#fff'}}>
       <HomePage navigation={navigation}/>
      <FlatData />
      <FlatDataList />
      <GraphPage />
      <BottomHome />
    </ScrollView>
  );
}

function MyStack() {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: { elevation: 1 }
  }}>
      <Stack.Screen name="DashBoard" component={DashBoard}
      options={({ navigation }) => ({
        title: 'DashBoard',
        headerLeft: () => (
         <Pressable 
         onPress={()=>navigation.navigate("Home")}>
 <Image style={{width:15,height:15, marginLeft:24}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
       
         </Pressable> 
         ),
      })} />

<Stack.Screen name="MyProfilePage" component={MyProfilePage}
      options={({ navigation }) => ({
        title: 'My Profile',
        headerLeft: () => (
         <Pressable 
         onPress={()=>navigation.navigate("Home")}>
 <Image style={{width:15,height:15, marginLeft:24}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
       
         </Pressable> 
         ),
      })} />

<Stack.Screen name="ProfileForm" component={ProfileForme}
      options={({ navigation }) => ({
        title: 'Update Profile ',
        headerLeft: () => (
         <Pressable 
         onPress={()=>navigation.navigate("Home")}>
 <Image style={{width:15,height:15, marginLeft:24}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
       
         </Pressable> 
         ),
         headerRight: () => (
          <Pressable 
          onPress={()=>navigation.goBack()}>
              <Text style={{color:'#1E6785',marginRight:10,fontWeight:'bold'}}>Go Back</Text>
          </Pressable> 
          ),
      })} />


      <Stack.Screen name="ProfileUpdate" component={ProfileUpdate}
      options={({ navigation }) => ({
        title: 'My Profile',
        headerLeft: () => (
         <Pressable 
         onPress={()=>navigation.goBack()}>
 <Image style={{width:15,height:15, marginLeft:24}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
       
         </Pressable> 
         ),
         headerRight: () => (
          <Pressable 
          onPress={()=>navigation.navigate('ProfileForm')}>
              <Text style={{color:'#1E6785',marginRight:10,fontWeight:'bold'}}>Edit</Text>
          </Pressable> 
          ),
      })} />
    </Stack.Navigator>
  );
}
function MyStack1() {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: { elevation: 1 }
  }}>
      <Stack.Screen name="DailyLogs" component={Dailylogs}
      options={({ navigation }) => ({
        title: 'DailyLogs',
        headerLeft: () => (
         <Pressable style={{width:40,height:40,marginLeft:12}}
         onPress={()=>navigation.navigate("Home")}>
 <Image style={{width:15,height:15, top:15,alignSelf:'center'}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
         </Pressable> 
         ),
         headerRight: () => (
   <View style={{flexDirection:'column',marginHorizontal:12}}>
                <Text style={{fontSize:16}}>05/03/2021</Text>
                <Text style={{fontSize:16,textAlign:'right'}}>16:16 pm</Text>
                </View>
          ),
      })} />
    </Stack.Navigator>
  );
}
function MyStack2() {
  return (
    <Stack.Navigator screenOptions={{
      headerStyle: { elevation: 1 }
  }}>
      <Stack.Screen name="Rx" component={Rx}
      options={({ navigation }) => ({
        title: 'My Plan, 3 March 2021',
        headerLeft: () => (
         <Pressable style={{width:40,height:40,marginLeft:12}}
         onPress={()=>navigation.navigate("Home")}>
 <Image style={{width:15,height:15,alignSelf:'center',top:12}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
         </Pressable> 
         ),
         headerRight: () => (
          <Image style={{width:27,height:27,position:'absolute',right:20,
          top: 15}} source={require('./Components/Image/calendar.png')} />
          ),
      })} />
       <Stack.Screen name="Diet" component={DietPropPage}
      options={({ navigation }) => ({
        title: 'My Plan, 3 March 2021',
        headerLeft: () => (
         <Pressable style={{width:40,height:40,marginLeft:12}}
         onPress={()=>navigation.goBack()}>
 <Image style={{width:15,height:15,alignSelf:'center',top:12}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
         </Pressable> 
         ),
         headerRight: () => (
          <Image style={{width:27,height:27,position:'absolute',right:20,
          top: 15}} source={require('./Components/Image/calendar.png')} />
          ),
      })} />
          <Stack.Screen name="MobileAuthPage" component={MobileAuthPage}
      options={({ navigation }) => ({
        title: 'Verify By Mobile No',
        headerLeft: () => (
         <Pressable style={{width:40,height:40,marginLeft:12}}
         onPress={()=>navigation.goBack()}>
 <Image style={{width:15,height:15,alignSelf:'center',top:12}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
         </Pressable> 
         ),
        
      })} />

<Stack.Screen name="VerificationCode" component={VerificationCode}
      options={({ navigation }) => ({
        title: 'Enter Verification Code',
        headerLeft: () => (
         <Pressable style={{width:40,height:40,marginLeft:12}}
         onPress={()=>navigation.goBack()}>
 <Image style={{width:15,height:15,alignSelf:'center',top:12}}
           source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
         </Pressable> 
         ),
        
      })} />
    </Stack.Navigator>
  );
}




export default function App() {
  const [authhome , setAuthhome] = useState(true)

  return (

 <NavigationContainer>
{/*       <Tab.Navigator
        initialRouteName="Home"
        activeColor="#1E6785"
        inactiveColor="#828282"
        barStyle={{ backgroundColor: '#fff' }}>
        <Tab.Screen name="Home" component={HomeScreen}
         options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24,backgroundColor:'#fff'}}  source={require('./Components/Image/Stethoscope.png')} />
          )
        }}
         />
        
        <Tab.Screen name="DailyLogs1" component={MyStack1}
         options={{
          tabBarLabel: 'DailyLogs',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Frame.png')} />
          )
        }}
         />
         <Tab.Screen name="Rx" component={MyStack2} 
         options={{
          tabBarLabel: 'Rx',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Medical_Annual.png')} />
          )
        }}
        />
          <Tab.Screen name="DashBoard" component={MyStack}
         options={{
          tabBarLabel: 'DashBoard',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Union.png')} />
          )
        }}
         />
        

      </Tab.Navigator>
   */}
  
  <BottomTabNav />
    </NavigationContainer>
    
    
     
  );
}
  
const styles = StyleSheet.create({  
  container: {  
    flex: 1,  
    justifyContent: 'center',  
    alignItems: 'center',  
    backgroundColor: '#F5FCFF',  
  },  
  welcome: {  
    fontSize: 20,  
    textAlign: 'center',  
    margin: 10,  
  },  
  instructions: {  
    textAlign: 'center',  
    color: '#333333',  
    marginBottom: 5,  
  },  
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    borderStyle: 'dashed',
    borderBottomColor: 'rgba(0, 0, 0, 0.12)',
    borderBottomWidth: 2,
    marginVertical: 10,
    paddingVertical: 10,
},
cardItem: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    padding: 5,
    width: 100,
    height: 'auto',
    borderRadius: 5,
    marginVertical: 5,
    
    shadowColor: 'rgba(236, 176, 45, 0.25)',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 2
},
activeCardItem: {
    borderColor: 'green',
    borderWidth: 2,
}
});  