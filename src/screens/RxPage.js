import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import DailyRoutine from '../components/RxPage/DailyRoutine'
import DailyRoutinePlan from '../components/RxPage/DailyRoutinePlan'

const RxPage = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <DailyRoutine />
            <DailyRoutinePlan />
        </ScrollView>
    )
}

export default RxPage

const styles = StyleSheet.create({})
