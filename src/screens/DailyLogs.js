import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import ReportPage from '../components/DailyLogsScreen/ReportPage'
import Symptoms from '../components/DailyLogsScreen/Symptoms'
import Vitals from '../components/DailyLogsScreen/Vitals'

const DailyLogs = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={{paddingHorizontal:12}}>
            <Symptoms />
           <Vitals />
           <ReportPage />
        </ScrollView>
    )
}

export default DailyLogs

const styles = StyleSheet.create({})
