import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import FlatDataList from '../../Components/FlatDataList'
import FlatData from '../components/HomeScreen/FlatData'
import HeaderPage from '../components/HomeScreen/HeaderPage'
import HomeBottomPage from '../components/HomeScreen/HomeBottomPage'
import HomeGraphPage from '../components/HomeScreen/HomeGraphPage'

const HomeScreen = ({navigation}) => {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <HeaderPage navigation={navigation}/>
            <FlatData />
            <FlatDataList />
            <HomeGraphPage />
            <HomeBottomPage />
        </ScrollView>
    )
}

export default HomeScreen

const styles = StyleSheet.create({})
