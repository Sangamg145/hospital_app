import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import WeeklyAdherance from '../components/DashBoard/WeeklyAdherance'

const DashBoard = () => {
    return (
        <View>
            <Text>Dashboard</Text>
            <WeeklyAdherance />
        </View>
    )
}

export default DashBoard

const styles = StyleSheet.create({})
