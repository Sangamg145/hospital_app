import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { StyleSheet, Text, View,Image } from 'react-native'
import HomeScreen from '../screens/HomeScreen';
import DailyLogs from '../screens/DailyLogs';

import RxPage from '../screens/RxPage';
import DashBoard from '../screens/DashBoard';
import HomeStackScreen from './HomeStackScreen';
import DailyLogsStack from './DailyLogsStack';
import RxStackScreen from './RxStackScreen';
import DashBoardStack from './DashBoardStack';
const Tab = createMaterialBottomTabNavigator();
const BottomTabNav = () => {
    return (
        <Tab.Navigator
        initialRouteName="HomeScreenTab"
        activeColor="#1E6785"
        inactiveColor="#828282"
        barStyle={{ backgroundColor: '#fff' }}>
        <Tab.Screen name="HomeScreenTab" component={HomeStackScreen}
         options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24,backgroundColor:'#fff'}}  source={require('../assets/images/Stethoscope.png')} />
          )
        }}
         />
        
        <Tab.Screen name="DailyLogsTab" component={DailyLogsStack}
         options={{
          tabBarLabel: 'DailyLogs',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('../assets/images/Frame.png')} />
          )
        }}
         />
         <Tab.Screen name="RxTab" component={RxStackScreen} 
         options={{
          tabBarLabel: 'Rx',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('../assets/images/Medical_Annual.png')} />
          )
        }}
        />
          <Tab.Screen name="DashBoardTab" component={DashBoardStack}
         options={{
          tabBarLabel: 'DashBoard',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('../assets/images/Union.png')} />
          )
        }}
         />
        

      </Tab.Navigator>
    
    )
}

export default BottomTabNav

const styles = StyleSheet.create({})
