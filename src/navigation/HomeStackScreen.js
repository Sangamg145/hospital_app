import React from 'react'
import { StyleSheet, Text, View,Pressable,Image } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import RxPage from '../screens/RxPage';
import HomeScreen from '../screens/HomeScreen';
const Stack = createStackNavigator();
        function HomeStackScreen() {
            return (
              <Stack.Navigator screenOptions={{
                headerStyle: { elevation: 1 }
            }}>
          <Stack.Screen name="HomeScreen" component={HomeScreen}
                options={({ navigation }) => ({
                  title: 'Update Profile ',
                  headerShown: false
                  
                })} />
          
          
                <Stack.Screen name="RxTab" component={RxPage}
                options={({ navigation }) => ({
                  title: 'My Profile',
                  headerLeft: () => (
                   <Pressable 
                   onPress={()=>navigation.goBack()}>
           <Image style={{width:15,height:15, marginLeft:24}}
                     source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
                 
                   </Pressable> 
                   ),
                   headerRight: () => (
                    <Pressable 
                    onPress={()=>navigation.navigate('DashBoardTab')}>
                        <Text style={{color:'#1E6785',marginRight:10,fontWeight:'bold'}}>Edit</Text>
                    </Pressable> 
                    ),
                })} />
              </Stack.Navigator>
            );
          }
export default HomeStackScreen

const styles = StyleSheet.create({})
