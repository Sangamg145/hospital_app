import React from 'react'
import { StyleSheet, Text, View,Pressable,Image } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import RxPage from '../screens/RxPage';
const Stack = createStackNavigator();
const RxStackScreen = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerStyle: { elevation: 1 }
        }}>
      <Stack.Screen name="Rx" component={RxPage}
            options={({ navigation }) => ({
                title: 'Rx Page',
                headerLeft: () => (
                 <Pressable 
                 onPress={()=>navigation.goBack()}>
         <Image style={{width:15,height:15, marginLeft:24}}
                   source={{uri:"https://cdn2.iconfinder.com/data/icons/pittogrammi/142/27-512.png"}} />
               
                 </Pressable> 
                 ),
                 headerRight: () => (
                  <Pressable 
                  onPress={()=>navigation.navigate('DashBoard')}>
                      <Text style={{color:'#1E6785',marginRight:10,fontWeight:'bold'}}>Edit</Text>
                  </Pressable> 
                  ),
              })} />
      
      
            
          </Stack.Navigator>
   
    )
}

export default RxStackScreen

const styles = StyleSheet.create({})
