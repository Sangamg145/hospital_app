import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,FlatList,ScrollView, Pressable } from 'react-native'
import { ActivityIndicator } from 'react-native-paper';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import axios from 'axios';
const Data1 = [{
    id:"1",
    name:"Diet",
    fill:30,
    image:require('../../assets/images/diet11.png'),
    a:'BreakFast (according to time)',
    b:'Warm Lemon Water',
    b1:'2 Glass',
    c:'Idli Sambar',
    c1:"Nos 2",
    d:'Fruit Chaat',
    d1:"2 Cup",
    name1:'Diet Completed'
 
 },
 {
    id:"2",
    name:"Exercise",
    fill:60,
    image:require('../../assets/images/weightlifting11.png'),
    a:'Before BreakFast',
    b:'Sit Ups 10 Sets',
    b1:'Special Instruction',
    c:'Lunges',
    c1:"5 Sets",
    name1:'Exercise Completed'
 },
 {
    id:"3",
    name:"Madication",
    fill:20,
    image:require('../../assets/images/pills11.png'),
    a:'after BreakFast',
    b:'Ecosprin - 75mg',
    b1:'Special Instruction',
    c:'Amlopress AT',
    c1:"As Doctor Instruction",
    name1:'Madication Completed'
   },
 {
    id:"4",
    name:"Wound Care",
    fill:70,
    image:require('../../assets/images/sling11.png'),
    a:'Before BreakFast',
    b:'Not Decided',
    b1:'Unknown',
    c:'no',
    c1:"unknows",
    name1:'Wound Care Completed'
   },
    {
       id:"5",
       name:"Diagnostic Test",
       fill:60,
       image:require('../../assets/images/Group11.png'),
       a:'Check Your Health',
       b:'Heart Check',
       b1:'Instruction',
       c:'Blood Check',
       c1:"unknown",
       name1:'Lab Test'
     },
    {
       id:"6",
       name:"Appointment",
       fill:50,
       image:require('../../assets/images/calendar11.png'),
       a:'Cunsult Doctor',
       b:'Previous Doctor ',
       b1:'Doctor Name',
       c:'Cunsult New',
       c1:"New Doctor Name",
       name1:'Doctors Appointment'
     },
 ]
const DailyRoutinePlan = ({navigation}) => {
    const [data12,setData12] = useState([])
    const [isLoading, setLoading] = useState(true);

    const renderItem = ({ item }) =>
     (
      <Pressable style={{width:164,height:168,paddingHorizontal:8,
         paddingVertical:12,borderRadius:10,marginBottom:20,backgroundColor:'#fff'
 }}
 onPress={()=>navigation.navigate("Diet", {
  itemId: item.name,
  a:item.a,
  b:item.b,
  b1:item.b1,
  c:item.c,
  c1:item.c1,
  d:item.d,
  d1:item.d1,
  name1:item.name1
})}>
      <View style={{paddingHorizontal:20}}>
      <AnimatedCircularProgress
     size={108}
     width={11}
     fill={item.fill}
     tintColor="#1E6785"
     backgroundColor="#D5ECF6"
     
      />
      <View style={{width:65,height:65,position:'absolute',left:40,top:20,justifyContent:'center'}}> 
      <Image style={{width:44,height:44,alignSelf:'center'}}  source={item.image} />
      </View>
    
      </View>
      <Text style={{textAlign:'center',marginTop:10,fontSize:18,fontWeight:'bold'}}>{item.name}</Text>
   </Pressable>
    );
    useEffect(() => {
      const URL = "http://13.126.80.252/doctor/api/plan-type"
      const Token = "57bbec1b20f1f33b5744090b26f3bed279239a35"
      const AuthStr = 'token '.concat(Token); 
      axios.get(URL, { headers: { Authorization: AuthStr } })
       .then(response => {
        setData12(response.data)
           console.log(response.data.status);
        })
        
       .catch((error) => {
           console.log('error ' + error);
        }); 
    }, []);
    return (
        <View>
           <Text style={{fontSize:16,marginTop:8,marginLeft:20,fontWeight:'bold'}}>Dr. Harshit Suneja (ENT)</Text>
  <Text style={{fontSize:12,marginTop:2,marginBottom:20,marginLeft:20}}>Max Hospital Saket</Text>
<View style={{backgroundColor:'rgba(212, 217, 232, 0.1)'}}>
<FlatList
        data={Data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        numColumns={2}
        columnWrapperStyle={{justifyContent: 'space-evenly'}}
      />
</View>
        </View>
    )
}

export default DailyRoutinePlan

const styles = StyleSheet.create({})
