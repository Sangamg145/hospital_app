import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,FlatList,ScrollView, Pressable } from 'react-native'
import { ActivityIndicator } from 'react-native-paper';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
const DailyRoutine = () => {
    return (
        <View>
        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:20}}>
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={80}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>M</Text>
      </View>
      
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={50}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
     <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={20}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
       <View style={{backgroundColor:'#1E6785',borderRadius:40,width:27,height:27,alignSelf:'center',marginTop:4}}>
       <Text style={{textAlign:'center',marginTop:4,color:'#fff'}}>W</Text>
       </View>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>T</Text>
      </View>
  
      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
          <Text style={{textAlign:'center',marginTop:8}}>F</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
         <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>

      <View style={{flexDirection:'column'}}>
      <AnimatedCircularProgress
    size={40}
    width={5}
    fill={0}
    tintColor="#1E6785"
    backgroundColor="#D5ECF6"
     />
        <Text style={{textAlign:'center',marginTop:8}}>S</Text>
      </View>
        </View>
  
        </View>
    )
}

export default DailyRoutine

const styles = StyleSheet.create({})
