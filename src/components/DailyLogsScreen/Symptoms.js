import React from 'react'
import { StyleSheet, Text, View,ScrollView,Pressable,Image, } from 'react-native'

const Symptoms = () => {
    return (
        <View>
                    <Text style={{fontSize:20,marginTop:16}}>Symptom</Text>
<ScrollView horizontal={true} showsHorizontalScrollIndicator={false} 
style={{flexDirection:'row',marginTop:12}}>
<Pressable style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}
 >
<Image source={require('../../assets/images/dailylog1.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>NauSea</Text>
</Pressable>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('../../assets/images/dailylog2.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Breathlessness</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('../../assets/images/dailylog3.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8,textAlign:'center'}}>Raising Heart Beat</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center'}}>
<Image source={require('../../assets/images/dailylog3.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8,textAlign:'center'}}>Raising Heart Beat</Text>
</View>
</ScrollView>

        </View>
    )
}

export default Symptoms

const styles = StyleSheet.create({})
