import React from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'

const ReportPage = () => {
    return (
        <View>
            
<Text style={{fontSize:20,marginTop:24,}}>My Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('../../assets/images/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('../../assets/images/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>



<Text style={{fontSize:20,marginTop:24,}}>Other Report</Text>
<View style={{flexDirection:'row',justifyContent:'space-between',borderColor:'rgba(77, 77, 79, 0.12)',borderBottomWidth:1}}>
<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('../../assets/images/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18 ,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>

<View style={{width:164,height:156,marginTop:20,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<View >
<View>
<View style={{width:48,height:48}}>
<Image source={require('../../assets/images/doc.png')} style={{width:22,height:27}} />
</View>
</View>
<Text style={{fontSize:18,fontWeight:'bold'}}>Cardiology</Text>
<Text style={{fontSize:12}}>Updated on 20/03/2021</Text>
<Text style={{fontSize:12}}>9 Files</Text>
</View>
</View>
</View>
       
        </View>
    )
}

export default ReportPage

const styles = StyleSheet.create({})
