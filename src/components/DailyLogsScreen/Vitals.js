import React from 'react'
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native'

const Vitals = () => {
    return (
        <View>
            <Text style={{fontSize:20,marginTop:24}}>Vitals</Text>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
<View style={{flexDirection:'row',marginTop:12}}>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('../../assets/images/dailylog4.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Blood Pressure</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('../../assets/images/dailylog5.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Temprature</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('../../assets/images/dailylog6.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Weight</Text>
</View>
<View style={{width:104,height:96,padding:8,justifyContent:'center',alignItems:'center',marginHorizontal:6,}}>
<Image source={require('../../assets/images/dailylog6.png')} style={{width:40,height:40}} />
<Text style={{fontSize:12,marginTop:8}}>Weight</Text>
</View>
</View>



        </ScrollView>
        </View>
    )
}

export default Vitals

const styles = StyleSheet.create({})
