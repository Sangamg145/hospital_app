import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,Image,ScrollView,Dimensions } from 'react-native'
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const WeeklyAdherance = ({navigation}) => {
    const [data1, setData1] = useState([])

useEffect(() => {
  const URL = "http://13.126.80.252/accounts/api/patient-detail/"
  const Token = "8f94fc3e9d82c09be0f1aebd1c4d49b211a9a549"
  const AuthStr = 'token '.concat(Token); 
  axios.post(URL, { headers: { Authorization: AuthStr } })
   .then(response => {
    setData1(response.data.status)
       console.log(response.data);
    })
    
   .catch((error) => {
       console.log('error ' + error);
    }); 
}, []);

    return (
        <View>
          <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:16,marginTop:16}}>
          <Text style={{fontSize:22,fontWeight:'bold'}}>Hello Harshit Suneja</Text>
        <Image style={{width:35,height:40,borderRadius:70,alignSelf:'center',resizeMode:'contain'}} 
          source={require('../../assets/images/Frame170.png')} />
        </View>
        <Text style={{fontSize:16,marginHorizontal:16}}>Keep Tracking Your Health</Text>

        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginHorizontal:16,marginTop:16}}>
          <View style={{width:122,height:108,flexDirection:'row'}}>
<Text style={{fontSize:72,color:'rgba(236, 176, 45, 1)',fontWeight:'bold'}}>85</Text>
<Text style={{fontSize:56,color:'rgba(236, 176, 45, 1)'}}>%</Text>
          </View>
         
        <View>
        <View style={{width:149,height:108,marginTop:4}}>
<Text style={{fontSize:20,color:'rgba(77, 77, 79, 1)',fontWeight:'bold'}}>This Week Adherances</Text>
<Text style={{fontSize:14,color:'rgba(77, 77, 79, 1)',marginVertical:20}}>5% Up from last Week</Text>
          </View>
        </View>
        </View>
        </View>
    )
}

export default WeeklyAdherance

const styles = StyleSheet.create({})
