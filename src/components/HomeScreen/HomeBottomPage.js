import React,{useState,useEffect} from 'react'
import { View, Text,Image,Animated,FlatList } from 'react-native'
import { Slider,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
const Data = [{
    id:1,
    name:"Swasthya Seva",
    spec:'Quality Care on Call',
    name1:"Start Call Now",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(49, 150, 199, 1)','rgba(71, 162, 206, 1)','rgba(132, 192, 220, 1)'],
    color2:'#fff',
    color3:'#fff',
    image:require('../../assets/images/image31.png'),
    wi:163,
    hi:122
},
{
    id:2,
    name:"Sanjivani",
    spec:'Accountable Hospitalization',
    name1:"Book Appointment",
    name2:"08:00 pm - 08:00 pm",
    color1:['rgba(89, 180, 209, 1)','rgba(128, 204, 220, 1)'],
    color2:'#1E6785',
    color3:'#000',
    image:require('../../assets/images/image14.png'),
    image1:require('../../assets/images/Drlogo.png'),
    wi:137,
    hi:127
},
{
    id:3,
    name:"Suraksha",
    spec:'Affordable Prevention',
    name1:"Book Lab Test",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(46, 49, 146, 1)','rgba(46, 49, 146, 1)'],
    color2:'#fff',
    color3:'#fff',
    image:require('../../assets/images/image15.png'),
    wi:178,
    hi:133
  },
]
export default function HomeBottomPage() {
        const [data1,setData1] = useState([])
    
        useEffect(() => {
            setData1(Data)
        }, [])

        const renderItem = ({ item }) => (
            <LinearGradient
            colors={item.color1}
            style={{width:264,height:248,backgroundColor:'#fff',borderRadius:5,padding:12,marginLeft:20,}}
            >
              <View >
        <Text style={{ width:239,
        height:28,
       fontWeight:'600',
fontSize: 24,
color:item.color2}}>{item.name}</Text>
        <Text style={{width:221,
        height:20,
        marginTop:4,
        fontWeight:'600',
        fontSize: 16,
        color:item.color2}}>{item.spec}</Text>
<View 
style={{ position:'absolute',
paddingHorizontal:12,
paddingVertical:8,
height: 36,
left: 12,
top: 68,
backgroundColor:"rgba(255, 255, 255, 1)",
borderRadius:5,
}}>
<Text style={{color:'#1E6785',fontWeight:'bold',fontSize:14,textAlign:'justify'}}>{item.name1}</Text>
  </View>
  <Image source={item.image} style={{width:item.wi,
        height:item.hi,
        left: 87.5,
        borderBottomRightRadius:5,
         top: 114,
         position:'absolute',
         justifyContent: 'center',
         alignItems: 'center',}} />
               </View>
                </LinearGradient>
 );
    return (
        <View>
        <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
            <View style={{width:164,height:232,backgroundColor:'#000000',padding:12,borderWidth:1,borderRadius:8,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#fff'}}>TBD</Text>
        <View style={{ width:20,height:20,backgroundColor:'#fff',borderRadius:20}}>
      </View>
</View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
      <Text style={{color:'#fff',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(250, 253, 253, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginTop:24}}>
        <Text style={{color:'#fff'}}>160</Text>
        <View style={{ borderStyle:'dotted', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>140</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>120</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text style={{color:'#fff'}}>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('../../assets/images/Vector1.png')} />
</View>
  
        </View>


        <View style={{width:164,height:232,backgroundColor:'#fff',borderWidth:1,borderRadius:8,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#000'}}>TBD</Text>
        <View style={{ width:20,height:20,backgroundColor:'#000',borderRadius:20}}>
      </View>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:10}}>
      <Text style={{color:'#000',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(0,0,0, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginLeft:-8,marginTop:28}}>
        <Text>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.3, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text>80</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text>70</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:14}}>
        <Text>60</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('../../assets/images/Vector2.png')} />

</View>
        </View>
        </View>


        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:30}}>
            <View style={{width:164,height:96,padding:12,backgroundColor:'#fff',borderWidth:1,borderRadius:5,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
        <Text style={{fontSize:20}}>Heart Rate</Text>
        </View>
        <View>
        <Image style={{width:28,height:28}}  source={require('../../assets/images/heart.png')} />
      </View>
</View>
<View style={{flexDirection:'row',width:132,height:36,alignItems:'center',marginTop:8}}>
<Text style={{fontSize:24}}>71</Text>
<Text style={{marginTop:5}}> BPM</Text>
</View>
        </View>



        <View style={{width:164,height:96,backgroundColor:'#fff',borderWidth:1,borderRadius:5,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View style={{flexDirection:'row',width:140,height:28,alignItems:'center'}}>
        <Text style={{fontSize:20}}>TBD</Text>
        </View>
        
</View>
<View style={{flexDirection:'row',width:132,height:36,alignItems:'center',marginTop:8}}>
<Text style={{fontSize:24}}>Reading</Text>
<Text style={{marginTop:5}}> Units</Text>
</View>
        </View>
        </View>


        <View style={{alignSelf:'center',marginTop:30}}>
        <View style={{width:343,height:240,backgroundColor:'#fff',borderWidth:1,borderRadius:5,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
        <Text style={{fontSize:22}}>How are you Feeling Today ?</Text>
        </View>    
        </View>
 <View style={{alignSelf:'center',marginTop:30}}>
 <Image style={{width:36,height:36,alignSelf:'center'}}  source={require('../../assets/images/emoji1.png')} />
 <Text>Very Happy</Text>
 </View>
 <View style={{flexDirection:'row',marginTop:30}}>
 <Image style={{width:24,height:24,marginLeft:0}}  source={require('../../assets/images/emoji2.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('../../assets/images/emoji3.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('../../assets/images/emoji5.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('../../assets/images/emoji5.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('../../assets/images/emoji6.png')} />
 </View>
 <View style={{marginTop:15}}>
 <Slider
    value={50}
    maximumValue={50}
    minimumValue={20}
    step={1}
    trackStyle={{ height: 10, backgroundColor: 'tomato' }}
    thumbStyle={{ height: 28, width: 28, backgroundColor: 'tomato' }}
    thumbProps={{
    }}/>
 </View>
        </View>
        </View>


        <View style={{marginTop:30,marginBottom:10,marginRight:3}}>
        <FlatList
        style={{marginBottom:20}}
        data={data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />        
        </View>
        </View>
    )
}
